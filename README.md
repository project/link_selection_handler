# Link Selection Handler

Link Selection Handler is a module that provides a field widget for link fields that allows configuration of autocomplete results when searching for internal links.
By default, Drupal link field widgets use a hard-coded entity autocomplete result, which would make finding nodes with the same title difficult.

This allows a link widget to utilize Entity Reference selection handlers such as provided by Views.

## Post-Installation

   1. Enable the module.
   2. Visit the “Manage form display” page of an entity with a link field.
   3. Choose “Link with selection handler” from the Widget select list.
   4. Press the “Edit” button (Gear icon).
   5. The additional configuration will be displayed similar to how Entity
      Reference field settings work.

## Known issues

   * The Reference type settings do not update when changing Reference method
     until after the Update button is pressed. To work around this, simply press
     the Update button after ensuring valid form values, then press the Edit
     button again and configure the Reference method settings. This can also be
     worked around by manually editing the configuration yaml with the
     appropriate configuration and re-synchronizing configuration. Please help
     fix this in the issue queue!
   * As this is its own field widget, other modules that provide alternative
     field widgets cannot be used in the same form display such as the useful
     [Link Attributes](https://drupal.org/project/link_attributes) module.

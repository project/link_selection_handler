<?php

namespace Drupal\link_selection_handler\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Element;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Link with selection handler field widget plugin.
 *
 * @FieldWidget(
 *   id = "link_selection_handler",
 *   label = @Translation("Link with selection handler"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkSelectionHandlerWidget extends LinkWidget implements ContainerFactoryPluginInterface {

  /**
   * The EntityReferenceSelection Plugin Manager.
   *
   * @var \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface
   */
  protected $selectionPluginManager;

  /**
   * Initialization method.
   *
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field plugin definition.
   * @param array $settings
   *   The configuration settings.
   * @param array $third_party_settings
   *   The configuration third-party settings.
   * @param \Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface $selectionPluginManager
   *   The plugin.manager.entity_reference_selection service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, SelectionPluginManagerInterface $selectionPluginManager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->selectionPluginManager = $selectionPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'match_operator' => 'CONTAINS',
      'match_limit' => 10,
      'selection_handler' => [
        'handler' => 'default',
        'handler_settings' => [
          'target_bundles' => NULL,
          'sort' => ['field' => 'label', 'direction' => 'ASC'],
          'auto_create' => FALSE,
          'auto_create_bundle' => '',
        ],
      ],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['match_operator'] = [
      '#type' => 'radios',
      '#title' => $this->t('Autocomplete matching'),
      '#default_value' => $this->getSetting('match_operator'),
      '#options' => $this->getMatchOperatorOptions(),
      '#description' => $this->t('Select the method used to collect autocomplete suggestions. Note that <em>Contains</em> can cause performance issues on sites with thousands of entities.'),
    ];

    $element['match_limit'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of results'),
      '#default_value' => $this->getSetting('match_limit'),
      '#min' => 0,
      '#description' => $this->t('The number of suggestions that will be listed. Use <em>0</em> to remove the limit.'),
    ];

    // Get all selection plugins for this entity type.
    $selection_plugins = $this->selectionPluginManager->getSelectionGroups('node');
    $handlers_options = [];

    $settings = $this->getSetting('selection_handler');

    foreach (array_keys($selection_plugins) as $selection_group_id) {
      if (array_key_exists($selection_group_id, $selection_plugins[$selection_group_id])) {
        $handlers_options[$selection_group_id] = Html::escape($selection_plugins[$selection_group_id][$selection_group_id]['label']);
      }
      elseif (array_key_exists($selection_group_id . ':node', $selection_plugins[$selection_group_id])) {
        $selection_group_plugin = $selection_group_id . ':node';
        $handlers_options[$selection_group_plugin] = Html::escape($selection_plugins[$selection_group_id][$selection_group_plugin]['base_plugin_label']);
      }
    }

    $element['selection_handler'] = [
      '#type' => 'container',
      '#process' => [[static::class, 'handlerSettingsAjaxProcess']],
      '#element_validate' => [[static::class, 'handlerSettingsValidate']],
      '#attributes' => [
        'data-link-selection-handler' => strtolower($this->fieldDefinition->getName()) . '-selection-handler',
      ],
    ];
    $element['selection_handler']['handler'] = [
      '#type' => 'details',
      '#title' => $this->t('Reference type'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#process' => [[static::class, 'formProcessMergeParent']],
    ];

    $element['selection_handler']['handler']['handler'] = [
      '#type' => 'select',
      '#title' => $this->t('Reference method'),
      '#options' => $handlers_options,
      '#default_value' => $settings['handler'],
      '#required' => TRUE,
      '#ajax' => TRUE,
      '#limit_validation_errors' => [],
    ];

    // @todo This is getting saved into field widget settings even though it is
    //   a submit button. DrupalWTF.
    $element['selection_handler']['handler']['handler_submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Change handler'),
      '#limit_validation_errors' => [],
      '#attributes' => [
        'class' => ['js-hide'],
      ],
      '#submit' => [[static::class, 'settingsAjaxSubmit']],
    ];

    $element['selection_handler']['handler']['handler_settings'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['entity_reference-settings']],
    ];
    $handler = $this->selectionPluginManager->getInstance([
      'target_type' => 'node',
      'handler' => $settings['handler'],
    ]);

    // @todo This does not get rebuilt appropriately when changing the handler
    //   for some reason. Neither JS or non-JS fallback will refresh the
    //   form. This is most-likely to do with the trying to do similar to
    //   what EntityReferenceItem is doing rather than coding it from
    //   scratch.
    $element['selection_handler']['handler']['handler_settings'] += $handler->buildConfigurationForm([], $form_state);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if ($this->supportsInternalLinks()) {
      $operators = $this->getMatchOperatorOptions();
      $summary[] = $this->t('Autocomplete matching: @match_operator', ['@match_operator' => $operators[$this->getSetting('match_operator')]]);
      $size = $this->getSetting('match_limit') ?: $this->t('unlimited');
      $summary[] = $this->t('Autocomplete suggestion list size: @size', ['@size' => $size]);
      $summary[] = $this->t('Selection handler: %handler', [
        '%handler' => $this->getSetting('selection_handler')['handler'],
      ]);
    }
    else {
      $summary[] = $this->t('Warning: Selection handler requires internal links');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    if (!$this->supportsInternalLinks()) {
      $this->messenger()->addWarning($this->t('Entity reference handler settings ignored as the field does not supprot internal links.'));
    }

    if ($element['uri']['#type'] === 'entity_autocomplete') {
      $settings = $this->getSetting('selection_handler');

      $handler_settings = $settings['handler_settings'] + [
        'match_operator' => $this->getSetting('match_operator'),
        'match_limit' => $this->getSetting('match_limit'),
      ];

      $element['uri']['#selection_handler'] = $settings['handler'];
      $element['uri']['#selection_settings'] = $handler_settings;
      $element['uri']['#validate_reference'] = FALSE;
    }

    return $element;
  }

  /**
   * Form element validation handler; Invokes selection plugin's validation.
   *
   * @param array $form
   *   The form where the settings form is being included in.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the (entire) configuration form.
   */
  public static function handlerSettingsValidate(array $form, FormStateInterface $form_state) {
    $handler_name = $form_state->getValue($form['handler']['handler']['#parents']);
    $handler = \Drupal::service('plugin.manager.entity_reference_selection')->getInstance([
      'target_type' => 'node',
      'handler' => $handler_name,
    ]);
    $handler->validateConfigurationForm($form['handler']['handler_settings'], $form_state);
  }

  /**
   * Render API callback: Processes the handler settings form.
   *
   * Allows access to the form state.
   *
   * @see static::settingsForm()
   */
  public static function handlerSettingsAjaxProcess(array $form, FormStateInterface $form_state): array {
    static::handlerSettingsAjaxProcessElement($form, $form_state->getCompleteForm());
    return $form;
  }

  /**
   * Adds entity_reference specific properties to AJAX form elements.
   *
   * From the settings form.
   *
   * @see static::handlerSettingsAjaxProcess()
   */
  public static function handlerSettingsAjaxProcessElement(array &$element, array $main_form): void {
    if (!empty($element['#ajax'])) {
      $element['#ajax'] = [
        'callback' => [static::class, 'settingsAjax'],
        'wrapper' => $main_form['#id'],
        'element' => $main_form['#array_parents'],
      ];
    }

    foreach (Element::children($element) as $key) {
      static::handlerSettingsAjaxProcessElement($element[$key], $main_form);
    }
  }

  /**
   * Merges form parents.
   *
   * Render API callback: Moves entity_reference specific Form API elements
   * (i.e. 'handler_settings') up a level for easier processing by the
   * validation and submission handlers.
   *
   * @see _entity_reference_field_settings_process()
   */
  public static function formProcessMergeParent(array $element): array {
    $parents = $element['#parents'];
    array_pop($parents);
    $element['#parents'] = $parents;
    return $element;
  }

  /**
   * Ajax callback for the handler settings form.
   *
   * @see static::settingsForm()
   */
  public static function settingsAjax($form, FormStateInterface $form_state) {
    return NestedArray::getValue($form, $form_state->getTriggeringElement()['#ajax']['element']);
  }

  /**
   * Submit handler for the non-JS case.
   *
   * @see static::settingsForm()
   */
  public static function settingsAjaxSubmit($form, FormStateInterface $form_state) {
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.entity_reference_selection')
    );
  }

  /**
   * Returns the options for the match operator.
   *
   * @return array
   *   List of options.
   */
  protected function getMatchOperatorOptions() {
    return [
      'STARTS_WITH' => $this->t('Starts with'),
      'CONTAINS' => $this->t('Contains'),
    ];
  }

}
